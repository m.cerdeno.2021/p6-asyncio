import asyncio

async def counter(id, time):
    print(f"Counter {id}: starting...")
    for i in range(time):
        await asyncio.sleep(1)
        print(f"Counter {id}: {i+1}")
        await asyncio.sleep(1)
    print(f"Counter {id}: finishing...")

async def main():
    await asyncio.gather(counter('A', 4), counter('B', 2), counter('C', 6))

asyncio.run(main())
