import asyncio

async def hello():
    print('¡Hola ...')
    await asyncio.sleep(1)
    print('... mundo!')
async def main():
    await asyncio.gather(hello(), hello(), hello())

loop = asyncio.get_event_loop()
try:
    loop.run_until_complete(main())
finally:
    loop.close()
